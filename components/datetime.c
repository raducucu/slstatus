/* See LICENSE file for copyright and license details. */
#include <stdio.h>
#include <time.h>

#include "../util.h"
#include "../decorations.h"

const char *
datetime(const char *fmt)
{
#ifdef USE_DECORATIONS
    static char decorated_fmt[256];
#endif
	time_t t;
    int rc;

	t = time(NULL);
#ifdef USE_DECORATIONS
    sprintf(decorated_fmt, CLOCK_SYM NARROW_SPACE "%s", fmt);
    rc = strftime(buf, sizeof(buf), decorated_fmt, localtime(&t));
#else
    rc = strftime(buf, sizeof(buf), fmt, localtime(&t));
#endif
	if (!rc) {
		warn("strftime: Result string exceeds buffer size");
		return NULL;
	}

	return buf;
}
