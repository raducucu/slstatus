/* See LICENSE file for copyright and license details. */
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "../util.h"
#include "../decorations.h"

const char *vpn_stat(void)
{
    FILE *pipe;
    static char lbuf[128];
    static char vpns_buf[256];
    int first_vpn = 1;

    pipe = popen("ag -o --nocolor cscotun[^\\s*] </proc/net/dev", "r" );
    if (pipe == NULL ) {
        warn("invoking %s failed: %s\n", buf, strerror(errno));
        return NULL;
    }

    while(!feof(pipe) ) {
        if( fgets(lbuf, 128, pipe) != NULL ) {
            lbuf[strlen(lbuf) - 1] = '\0';
            if (first_vpn) {
                snprintf(vpns_buf, sizeof(vpns_buf), "%s", lbuf);
                first_vpn = 0;
            } else {
                snprintf(vpns_buf, sizeof(vpns_buf), ", %s", lbuf);
            }
        }
    }

    if (first_vpn) {
        sprintf(vpns_buf, "-");
    }

    /* Close pipe */
    (void)pclose(pipe);

#ifdef USE_DECORATIONS
    return bprintf(VPN_ACCOUNT_SYM NARROW_SPACE "%s", vpns_buf);
#else
    return bprintf("%s", vpns_buf);
#endif
}
